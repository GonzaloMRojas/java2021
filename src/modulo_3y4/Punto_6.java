package modulo_3y4;

import java.util.Scanner;

public class Punto_6 {

	public static void main(String[] args) {

		int curso;
		
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Ingrese el nombre del sujeto: ");
		String nombre = scan.nextLine();
		System.out.print("Ingrese los a�os de estudio del sujeto: ");
		curso = scan.nextInt();
		
		if(curso>=0) {
			if(curso==0) {
				System.out.println(nombre+" lleva "+curso+" a�os estudiando");
			}
			else if(curso>12){
				System.out.println(nombre+" lleva "+curso+" a�os estudiando");
			}
			else if(curso<=12){
				if(curso>=7){
					System.out.println(nombre+" lleva "+curso+" a�os estudiando");
				}
				else if(curso<=6) {
					if(curso>=1) {
						System.out.println(nombre+" lleva "+curso+" a�os estudiando");
					}
					else {
						System.out.println(nombre+" no estudia o no ingres� un a�o correcto");
					}
				}
			}
		}
		else {
			System.out.println(nombre+" no estudia o no ingres� un a�o correcto");
		}
		scan.close();

	}

}
