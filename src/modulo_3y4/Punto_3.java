package modulo_3y4;

import java.util.Scanner;

public class Punto_3 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Ingrese el mes: ");
		String mes = scan.nextLine();
		
		if (mes.equals("Febrero")) {
			System.out.println(mes + " tiene 28 d�as.");
		}
		else if(mes.equals("Abril")){ 
			System.out.println(mes + " tiene 30 d�as.");
		}
		else if (mes.equals("Junio")) {
			System.out.println(mes + " tiene 30 d�as.");
		}
		else if (mes.equals("Septiembre")) {
			System.out.println(mes + " tiene 30 d�as.");
		}
		else if (mes.equals("Noviembre")) {
			System.out.println(mes + " tiene 30 d�as.");
		}
		else if (mes.equals("Enero")) {
			System.out.println(mes + " tiene 31 d�as.");
		}
		else if (mes.equals("Marzo")) {
			System.out.println(mes + " tiene 31 d�as.");
		}
		else if (mes.equals("Mayo")) {
			System.out.println(mes + " tiene 31 d�as.");
		}
		else if (mes.equals("Julio")) {
			System.out.println(mes + " tiene 31 d�as.");
		}
		else if (mes.equals("Agosto")) {
			System.out.println(mes + " tiene 31 d�as.");
		}
		else if (mes.equals("Octubre")) {
			System.out.println(mes + " tiene 31 d�as.");
		}
		else if (mes.equals("Diciembre")) {
			System.out.println(mes + " tiene 31 d�as.");
		}
		else {
			System.out.println("No ingres� un mes correcto (o su inicial est� en min�scula)");
		}
		scan.close();
	}
}