package modulo_3y4;

import java.util.Scanner;

public class Punto_12 {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		
		int num;
		System.out.println("Usted ingresar� un n�mero, seg�n el n�mero que sea, se le indicar� la docena correspondiente");
		System.out.print("Ingrese su n�mero: ");
		num = scan.nextInt();
		
		if(num>=1 && num<=12) {
			System.out.println("El n�mero \"" + num + "\" est� dentro de la docena \"1\"");
		}
		else if(num>=13 && num<=24) {
			System.out.println("El n�mero \"" + num + "\" est� dentro de la docena \"2\"");
		}
		else if(num>=25 && num<=36) {
			System.out.println("El n�mero \"" + num + "\" est� dentro de la docena \"3\"");
		}
		else if(num<1 || num>36) {
			System.out.println("El n�mero \"" + num + "\" est� fuera rango");
		}
		scan.close();

	}

}
