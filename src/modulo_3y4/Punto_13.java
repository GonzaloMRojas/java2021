package modulo_3y4;

import java.util.Scanner;

public class Punto_13 {

	public static void main(String[] args) {
		
Scanner scan = new Scanner(System.in);
		
		
		System.out.print("Ingrese el mes del cual quiere saber su cantidad de d�as: ");
		String mes = scan.nextLine();
		
		switch(mes){
		case "Febrero": case "febrero":
			System.out.println("Tiene 28 dias");
			break;
		case "Abril": case "abril": case "Junio": case "junio": case "Septiembre": case "septiembre": case "Noviembre": case "noviembre":
			System.out.println("Tiene 30 d�as");
			break;
		case "Enero": case "Marzo": case "marzo": case "Mayo": case "mayo": case "Julio": case "julio": case "Agosto": case "agosto": case "Octubre": case "octubre": case "Diciembre": case "diciembre":
			System.out.println("Tiene 31 d�as");
			break;
		default:
			System.out.println("Ingresaste un mes incorresto o no ingresaste algo");
		}
		scan.close();

	}

}
